<?php
	$thisfile=basename(__FILE__, '');
	$configs = include realpath(dirname(__FILE__)).'/config.php';
	require_once $configs['libPath'].'KLogger.php';

	set_time_limit($configs['php_time_limit']);

	$log = new KLogger ( $configs['dataPath'].'log/log.txt' , KLogger::DEBUG );

	$host_machine_addr = $configs['host_machine_addr'];
	$port = $configs['port'];
	$scoring_machine_addr = $configs['scoring_machine_addr'];
	
	$log->logInfo("$thisfile: Manual Scoring is called.");
	$batchOfRequests =array(
						"17620013952487",
						"17640500701692",
						"17640500694875",
						"17640519961217",
						"17620013214185",
						"17640522110600",
						"17640522055816",
						"17640522198600",
						"17640522202539",
						"14430126159575",
						"14430135226399"
					);
	foreach($batchOfRequests as $singleRequest){
		$singleRequest = serialize($singleRequest);
		call_posting_func($port, $host_machine_addr, $scoring_machine_addr, $singleRequest);
	}

function call_posting_func($port, $host_machine_addr, $scoring_machine_addr, $singleRequest)
{
	global $log;
	$thisfile=basename(__FILE__, '');
	$socket = socket_create(AF_INET, SOCK_STREAM, 0);
	if($socket == False){
		$log->logError($singleRequest.": $thisfile: Socket couldn't be created.");
		return -1;
	}
	else{
		$log->logInfo($singleRequest.": $thisfile: Socket created successfully.");
		$isSocketConnected =socket_connect($socket, $scoring_machine_addr, $port);
		if(!$isSocketConnected){
			$log->logError($singleRequest.": $thisfile: Error writing candidate on port. Skipping Candidate.");
			return -1;
		}
		else{
			$log->logInfo($singleRequest.": $thisfile: candidate written on port successfully");
		}
		socket_write($socket,$singleRequest);
		sleep(1);
		socket_close($socket);
		return 1;
	}
}
?>
