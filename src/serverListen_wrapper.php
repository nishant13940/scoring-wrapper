<?php
	$thisfile=basename(__FILE__,'');
	$configs = include realpath(dirname(__FILE__)).'/config.php';
	require_once $configs['libPath'].'KLogger.php';
	require_once $configs['libPath'].'notifyError.php';
	require_once $configs['srcPath'].'callCSScoring.php';
	require_once $configs['srcPath'].'callSVARScoring.php';
	require_once $configs['srcPath'].'callCSDelivery.php';
	require_once $configs['libPath'].'updateScores.php';
	require_once $configs['libPath'].'common_functions.php';
	require_once $configs['libPath'].'scoringStatus.php';

	set_time_limit($configs['php_time_limit']);

	$log = new KLogger ( $configs['dataPath'].'log/log.txt' , KLogger::DEBUG );
	
	$port = $configs['port'];
	$host_machine_addr = $configs['host_machine_addr'];
	listenSocket($host_machine_addr,$port);

	function listenSocket($host_machine_addr, $port){
		global $log,$configs;
		$thisfile=basename(__FILE__,'');
		$socket = socket_create(AF_INET, SOCK_STREAM, 0);
		if($socket==False){
			$log->logError("$thisfile: Socket couldn't be created.");
			return -1;
		}
		else{
			if(!socket_bind($socket, $host_machine_addr, $port)){
				$log->logError("$thisfile: Could not bind to socket.");
				return -1;
			}
			else{
				if(!socket_listen($socket, 3)){
					$log->logError("$thisfile: Could not set up socket listener.");
					return -1;
				}
				else{				
					$log->logInfo("$thisfile: Listening Socket.");
				}
			}
		}
		while(true){
			$spawn = socket_accept($socket);
			$input = socket_read($spawn, 2048);			
			$log->logDebug($input.": $thisfile: Socket request recieved to handle.");
			if (strpos($input,$configs['keywordForCrowdScore']) !== false) {
    			$log->logInfo("$input : This is a update crowdsorce scores request.");
    			$candId = explode("=",$input);
				$candId = trim($candId[1]);
				updateScoringStatus($candId,$configs['scoringStatus_forFS'], $configs['scoringStatus_CSScoringDone'], "");
				$isSuccess = updateCorpMISScores($candId);
				if($isSuccess){
					updateScoringStatus($candId,$configs['scoringStatus_forFS'], $configs['scoringStatus_scoreUpdate'], "");
				}
			}
			else if(strpos($input,$configs['keywordForSVARScore']) !== false){
				
				$log->logInfo("$input : This is a update SVAR scores request.");			
				$candId = explode("=",$input);
				$candId = trim($candId[1]);
				updateScoringStatus($candId,$configs['scoringStatus_forRSLR'], $configs['scoringStatus_RSLRScoringDone'], "");
				$isSuccess = updateCorpMISScores($candId);
				if($isSuccess){
					updateScoringStatus($candId,$configs['scoringStatus_forRSLR'], $configs['scoringStatus_scoreUpdate'], "");
				}
			}
			else{
				$input_dec = unserialize($input);
				if ($input == 'quit') {
					socket_close($spawn);
					break;
				}
				else{
					$isString = is_string($input_dec);
					if(!$isString){
						updateScoringStatus($input_dec['candId'],$configs['scoringStatus_forFS'], $configs['scoringStatus_CSDeliveryDone'],"");
						$log->logDebug($input_dec['candId'].": $thisfile: This is a crowdsource input. Sending it to CS scoring...");
						$isSuccessfullyCalled = call_scoring_func($configs['scoring_engine_host_port'], $host_machine_addr,$configs['scoring_engine_host_addr'], $input);
						if($isSuccessfullyCalled){
							updateScoringStatus($input_dec['candId'],$configs['scoringStatus_forFS'], $configs['scoringStatus_CSScoringCalled'], "");
							$log->logInfo($input_dec['candId'].": $thisfile: Candidate written on crowdsource scoring port successfully.");
						}
						else{
							$log->logError($input_dec['candId'].": $thisfile: Could not write candidate information crowdsource scoring port.");
						}
					}
					else{
						$log->logDebug($input_dec.": $thisfile: This is a fresh request. Sending it to run scoring...");
						$toScore = whatToScoreForThisCandidate($input_dec);
						$scoreSVAR 	=	$toScore['RSLR'];
						$scoreCS 	=	$toScore['FS'];
						
						if($scoreSVAR){
							updateScoringStatus($input_dec, $configs['scoringStatus_forRSLR'], $configs['scoringStatus_RSLRScoringCalling'], "");
							$isSuccessfullyCalled = call_svar_scoring_func($configs['svar_scoring_host_port'], $host_machine_addr,$configs['svar_scoring_host_addr'], $input_dec);
							if($isSuccessfullyCalled){
								updateScoringStatus($input_dec, $configs['scoringStatus_forRSLR'], $configs['scoringStatus_RSLRScoringCalled'], "");
								$log->logInfo($input_dec.": $thisfile: Candidate written on svar scoring port successfully.");
							}
							else{
								$log->logError($input_dec.": $thisfile: Could not write candidate information svar scoring port.");
							}
						}
						if($scoreCS){
							updateScoringStatus($input_dec, $configs['scoringStatus_forFS'], $configs['scoringStatus_CSDeliveryCalling'], "");
							$isSuccessfullyCalled = call_cs_delivery_func($configs['cs_delivery_host_port'], $host_machine_addr,$configs['cs_delivery_host_addr'], $input_dec);
							if($isSuccessfullyCalled){
								updateScoringStatus($input_dec, $configs['scoringStatus_forFS'], $configs['scoringStatus_CSDeliveryCalled'], "");
								$log->logInfo($input_dec.": $thisfile: Candidate written on cs delivery port successfully.");
							}
							else{
								$log->logError($input_dec.": $thisfile: Could not write candidate information on cs delivery port.");
							}
						}
					}
				}
			}
		}
		socket_close($socket);
	}
?>
