<?php
	$thisfile=basename(__FILE__, '');
	$configs = include realpath(dirname(__FILE__)).'/config.php';
	require_once $configs['libPath'].'KLogger.php';

	set_time_limit($configs['php_time_limit']);

	$log = new KLogger ( $configs['dataPath'].'log/log.txt' , KLogger::DEBUG );


	function call_scoring_func($port, $host_machine_addr, $scoring_machine_addr, $serializedCandInfo)
	{
		global $log;
		$thisfile=basename(__FILE__, '');
		$candInfo = unserialize($serializedCandInfo);
		$socket = socket_create(AF_INET, SOCK_STREAM, 0);
		if($socket == False){
			$subject= "Crowdsource Error | Error Calling CS Scroing For ".$candInfo;
			$message = $candInfo->candId.": $thisfile: Error writing candidate on CS scring port. Socket could not be created. Skipping Candidate. SVARID: ".$candInfo;
			notifyHumans($subject, $message);
			$log->logError($message);
			insert("error", array(
									"RaisedAt=NOW()",
									"QueryString=''",
									"Message='".$message."'"
									));
			return 0;
		}
		else{
			$log->logInfo($candInfo['candId'].": $thisfile: Socket created successfully.");
			$isSocketConnected =socket_connect($socket, $scoring_machine_addr, $port);
			if(!$isSocketConnected){
				$subject= "Crowdsource Error | Error Calling Scroing For ".$candInfo['candId'];
				$message = $candInfo['candId'].": $thisfile: Error writing candidate on CS port. Skipping Candidate. CrowdInput: ".serialize($candInfo);
				notifyHumans($subject, $message);
				$log->logError($message);
				insert("error", array(
										"RaisedAt=NOW()",
										"QueryString=''",
										"Message='".$message."'"
										));
				return 0;
			}
			else{
				$log->logInfo($candInfo['candId'].": $thisfile: candidate written on cs scoring port successfully");
			}
			if(!empty($candInfo)){
				socket_write($socket,serialize($candInfo));
				sleep(1);
				socket_close($socket);
				return 1;
			}
			else{
				$message = $thisfile.": Candidate with empty seriaized array was  written on scoring wrapper. Skipping. CrowdInput: ".serialize($candInfo);
				$log->logError($message);
				return 0;
			}
		}
	}
?>
