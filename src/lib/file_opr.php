<?php
	$thisfile=basename(__FILE__, '');
	$configs = include realpath(dirname(__FILE__)).'/../config.php';
	require_once $configs['libPath'].'KLogger.php';
	require_once $configs['libPath'].'db.php';
	set_time_limit($configs['php_time_limit']);
	$log = new KLogger ( $configs['dataPath'].'log/log.txt' , KLogger::DEBUG );
	
	function recurse_copy($src,$dst) {
		global $log;
		$thisfile=basename(__FILE__, '');
		if (!file_exists($src)){
			$log->logError("Can't Copy. $src doesn't exists. Exiting.");
			return 0;
		}
		$isSuccessful=1;
		$dir = opendir($src);
		@mkdir($dst); 
		while(false !== ( $file = readdir($dir)) ) { 
			if (( $file != '.' ) && ( $file != '..' )) { 
				if ( is_dir($src . '/' . $file) ) { 
				recurse_copy($src . '/' . $file,$dst . '/' . $file); 
				} 
				else {
					$isSuccessful = $isSuccessful  & copy($src . '/' . $file,$dst . '/' . $file); 
					if(!$isSuccessful){
						break;
					}
				} 
			} 
		}	
		closedir($dir); 
		return $isSuccessful;
	} 

	function inputFile($candToProcess){
		global $configs;
		$thisfile=basename(__FILE__, '');
		try{
			$line=$candToProcess['candId']."\t".$candToProcess['quesId']."\t".$candToProcess['moduleId']."\t";
			$template=select("template",array("name='inputFile'"));
			foreach($template as $key => $value){
				$inputFileLocation = $configs['dataPath'].$configs['inputSubPath'].$candToProcess['candId'].".input";
				$value['text']=(implode("\t",explode("\\t",$value['text'])));
				file_put_contents($inputFileLocation,implode("\t",explode("\t",$value['text']))."\n".$line);
			}
			return 1;
		}
		catch(Exception $e){
			$log->logError($candToPrcess['candId'].": $thisfile: Error creating Input File. $e->getMessage()");
			return 0;
		}
	}
	
	function propertiesFile($candToProcess){
		global $log,$configs;
		$thisfile=basename(__FILE__, '');
		try{
			$template=select("template",array("name='propertiesFile'"));
			foreach($template as $key => $value){
				$propertiesFileLocation = $configs['dataPath'].$configs['propertiesSubPath'].$candToProcess['candId'].".properties";
				$value['text']=str_replace("###numAssign###",$candToProcess['numAssign'],str_replace("###reward###",$candToProcess['reward'],str_replace("###title###",$configs['hit_title'],str_replace("<br/>","\r\n",$value['text']))));
				
				file_put_contents($propertiesFileLocation,$value['text']);
			}
			return 1;
		}
		catch(Exception $e){
			$log->logError($candToPrcess['candId'].": $thisfile: Error creating properties file. $e->getMessage()");
			return 0;
		}
	}
	
	function questionFile($candToProcess){
		global $log,$configs;
		$thisfile=basename(__FILE__, '');
		try{
			$template=select("template",array("name='questionFile'"));
			foreach($template as $key => $value){
				$questionFileLocation = $configs['dataPath'].$configs['questionSubPath'].$candToProcess['candId'].".question";
				$value['text']=str_replace("###hit_url###",$configs['hit_url'],str_replace("<br/>","\r\n",$value['text']));
			
				file_put_contents($questionFileLocation,$value['text']);
			}
			return 1;
		}
		catch(Exception $e){
			$log->logError($candToPrcess['candId'].": $thisfile: Error creating question file. $e->getMessage()");
			return 0;
		}
	}
	
	function copySample($candToProcess){
		global $log, $configs;
		$thisfile=basename(__FILE__, '');
		$tpod = substr($candToProcess['candId'],0,8);
		if(is_url_exist($configs['corpMIS_url'].$tpod.$candToProcess['candId'].".wav")){
			echo("URL exists at corpMIS.");
		}
		else{
			echo("URL doesn't exists at corpMIS.");
			if(recurse_copy($configs['candidateDataServer'].$candToProcess['candId'], $configs['dataPath'].$configs['sampleSubPath'].$candToProcess['candId'])){
				$log->logDebug($candToProcess['candId'].": $thisfile: Samples copied successfully.");
				return 1;
			}
			else{
				$log->logError($candToProcess['candId'].": $thisfile: Error copying samples.");
				return 0;
			}
		}
	}
	
	function is_url_exist($url){
		$ch = curl_init($url);    
		curl_setopt($ch, CURLOPT_NOBODY, true);
		curl_exec($ch);
		$code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		if($code == 200){
			$status = true;
		}
		else{
			$status = false;
		}
		curl_close($ch);
		return $status;
	}
?>
	
			
