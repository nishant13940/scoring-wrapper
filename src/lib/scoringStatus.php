<?php
	$configs = include realpath(dirname(__FILE__)).'/../config.php';
	require_once $configs['libPath'].'KLogger.php';
	require_once $configs['libPath'].'db.php';
	
	$log = new KLogger ( $configs['dataPath'].'log/log.txt' , KLogger::DEBUG );	
	
	function initializeScoringStatus($candId){
		global $configs,$log;
		insert("scoringStatus",array(
								"candidateId='".$candId."'",
								"SVAR=''",
								"FS=''",
								"message=''"
					));
	}
	
	function updateScoringStatus($candId,$forWhom, $status, $optionalMessage){
		global $configs,$log;
		$other = $forWhom==$configs['scoringStatus_forRSLR']?$configs['scoringStatus_forFS']:$configs['scoringStatus_forRSLR'];
		insert("scoringStatus",array(
								"candidateId='".$candId."'",
								$forWhom."='".$status."'",
								$other."='-1'",
								"message='".$optionalMessage."'"
								));
	}
?>
