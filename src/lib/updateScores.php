<?php
	$configs = include realpath(dirname(__FILE__)).'/../config.php';
	require_once $configs['libPath'].'KLogger.php';
	require_once $configs['libPath'].'db.php';
	require_once $configs['libPath'].'db_central.php';
	require_once $configs['libPath'].'common_functions.php';
	require_once $configs['libPath'].'notifyError.php';
	require_once $configs['libPath'].'curlScript.php';
	
	$log = new KLogger ( $configs['dataPath'].'log/log.txt' , KLogger::DEBUG);	
	
	function updateCorpMISScores($candId){
		global $log,$configs;
		$thisfile=basename(__FILE__,'');
		$toScore = whatToScoreForThisCandidate($candId);
		$url="";

		if($toScore["RSLR"] && !$toScore["FS"]){
			$url = getSVAROnlyURL($candId);
			
		}
		else if(!$toScore["RSLR"] && $toScore["FS"]){
			$url = getCSOnlyURL($candId);
		}
		else if($toScore["RSLR"] && $toScore["FS"]){
			$url = getSVAR_CSURL($candId);
		}
		if($url!=''){
			echo($url);
			$is_ok = http_response($candId, $configs['corpmisScoreUpdateURL'],$url);
			if($is_ok!=False){
				$log->logInfo("$candId: Scores has been updated on corpmis.");
				return 1;
			}
			else{
				$subject = "Scoring Wrapper | update CorpMIS score error";
				$message = $candId.": ".$thisfile.": Error in updating scores on corpmis, http_response: ".$is_ok;
				notifyHumans($subject, $message);
				$log->logError($message);
				return 0;
			}
		}
		else{
			$subject = "Scoring Wrapper | CorpMIS url Empty Error";
			$message = $candId.": ".$thisfile.": Error in updating scores on corpmis because url for candidate is empty.";
			notifyHumans($subject, $message);
			$log->logError($message);
			return 0;
		}
	}
	
	
	function getSVAROnlyURL($candId){
		global $log,$configs;
		$thisfile=basename(__FILE__,'');
		$rslrArray = selectCentral("tbl_SVARScores_RSLR",array(
																"SVARID='".$candId."'"));
		if(empty($rslrArray)){
			$subject = "Scoring Wrapper | SVAR Only update CorpMIS score error";
			$message = $candId.": ".$thisfile.": SVAR update CorpMIS request recieved but couldn't find candidate entry on central db.";
			notifyHumans($subject, $message);
			$log->logError($message);
			//Calling SVARScoring Again.
			$isSuccessfullyCalled = call_svar_scoring_func($configs['svar_scoring_host_port'], $configs['host_machine_addr'], $configs['svar_scoring_host_addr'], $candId);
			if($isSuccessfullyCalled){
				//updateScoringStatus($candId,$forWhom, $status, $optionalMessage)
				$log->logInfo($input_dec.": $thisfile: Candidate written on svar scoring port successfully.");
			}
			else{
				$log->logError($input_dec.": $thisfile: Could not write candidate information svar scoring port.");
			}
			return '';
		}
		else{
			if($rslrArray[0]['corpmis']==''){
				$subject = "Scoring Wrapper | SVAR Only update CorpMIS score error";
				$message = $candId.": ".$thisfile.": SVAR update CorpMIS request recieved but corpmis field for candidate is empty";
			}
			$url = $rslrArray[0]['corpmis']!=''?"updatePwd=updateScore".$rslrArray[0]['corpmis']:'';
			return $url;
		}
	}
	
	function getCSOnlyURL($candId){
		global $log,$configs;
		$thisfile=basename(__FILE__,'');
		$fsArray = selectCentral("tbl_SVARScores_FS",array(
																"SVARID='".$candId."'"));
			if(empty($fsArray)){
				$subject = "Scoring Wrapper | SVAR Only update CorpMIS score error";
				$message = $candId.": ".$thisfile.": SVAR update CorpMIS request recieved but couldn't find candidate entry on central db.";
				notifyHumans($subject, $message);
				$log->logError($message);
				return '';
			}
			else{
				$url = $fsArray[0]['corpmis']!=''?"updatePwd=updateScore".$fsArray[0]['corpmis']:'';
				return $url;
			}
		
	}
	
	function getSVAR_CSURL($candId){
		global $log,$configs;
		$thisfile=basename(__FILE__,'');
		$rslrArray = selectCentral("tbl_SVARScores_RSLR",array("SVARID='".$candId."'"));
		$fsArray = selectCentral("tbl_SVARScores_FS",array("SVARID='".$candId."'"));
		if(empty($rslrArray)){
			$rslrAwaiting = selectCentral("templates",array("name='rslrAwaiting'"));
			$rslrString = $rslrAwaiting[0]['text'];
			$wasRSLRPresent = 0;
		}
		else{
			$rslrString = $rslrArray[0]['corpmis'];
			$wasRSLRPresent = 1;
		}
		
		if(empty($fsArray)){
			$fsAwaiting = selectCentral("templates",array("name='fsAwaiting'"));
			$fsString = $fsAwaiting[0]['text'];
			$wasFSPresent = 0;
		}
		else{
			$fsString = $fsArray[0]['corpmis'];
			$wasFSPresent = 1;
			
		}

		if($wasRSLRPresent){
			$url = "updatePwd=updateScore".$rslrString.$fsString;
			return $url;
		}
		else if($wasFSPresent){
			$url = "updatePwd=updateScore".$fsString.$rslrString;
			return $url;
		}
		else{
			return '';
		}
		
	}
	
	//updateCorpMISScores("14430077958870");
	updateCorpMISScores("17640522202539");
?>
