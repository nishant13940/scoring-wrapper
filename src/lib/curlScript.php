<?php 
	$configs = include realpath(dirname(__FILE__)).'/../config.php';
	require_once $configs['libPath'].'KLogger.php';
	require_once $configs['libPath'].'db.php';
	
	$log = new KLogger ( $configs['dataPath'].'log/log.txt' , KLogger::DEBUG);	
	
function http_response($candId, $url, $postfield, $status = null, $wait = 3) 
{ 
	global $log,$configs;
	$thisfile=basename(__FILE__,'');
        $time = microtime(true); 
        $expire = $time + $wait; 
		// we are the parent 
		$ch = curl_init(); 
		curl_setopt($ch, CURLOPT_URL, $url); 
		curl_setopt($ch, CURLOPT_HEADER, FALSE); 
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS,$postfield);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE); 
		$head = curl_exec($ch);
		if(curl_errno($ch)){
			$subject = "Scoring Wrapper | update CorpMIS score error";
			$message = $candId.": ".$thisfile.": Error in updating scores on corpmis, curl_error: ".curl_error($ch);
			notifyHumans($subject, $message);
			$log->logError($message);
		} 
		$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE); 
		echo($head);
		$xml = simplexml_load_string($head);
		$json = json_encode($xml);
		$array = json_decode($json,TRUE);
		if($array['errorStatus']!=$configs['corpMISsuccessfulUpdateCode']){
			insert("post2ApiError",array(
										"svarID='".$candId."'",
										"errorStatus='".$array['errorStatus']."'",
										"errorMessage='".$array['errorMessage']."'",
										"Date=NOW()",
										"serverIP='".$configs['host_machine_addr']."'",
										"serverName='".$configs['host_machine_name']."'",
										"status=''",
										"postString='".serialize($postfield)."'"
										));
			return FALSE;
		}
		curl_close($ch); 
		if(!$head) 
		{ 
			return FALSE; 
		} 
		
		if($status === null) 
		{ 
			if($httpCode < 400) 
			{ 
				return TRUE; 
			} 
			else 
			{ 
				return FALSE; 
			} 
		} 
		elseif($status == $httpCode) 
		{ 
			return TRUE; 
		} 
		
		return FALSE;  
    } 
?>
