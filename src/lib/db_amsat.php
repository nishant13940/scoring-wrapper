<?php
	$thisfile=basename(__FILE__, '');
	$configs = include realpath(dirname(__FILE__)).'/../config.php';
	require_once $configs['libPath'].'KLogger.php';
	set_time_limit($configs['php_time_limit']);
	$log = new KLogger ( $configs['dataPath'].'log/log.txt' , KLogger::DEBUG );
	
	function conn_dbamsat(){
		global $configs,$log,$thisfile;
		$con= mysqli_connect($configs['host_db'],$configs['uname_db'],$configs['pass_db'],$configs['db_amsat']);
		if (mysqli_connect_errno()) {
			$log->logError("$thisfile: Failed to connect to MySQL: " . mysqli_connect_error());
		}
		return $con;
	}
	
	function closeConn_dbamsat($con){
		mysqli_close($con);
		return 1;
	}
	
	function select_dbamsat($tblName, $whereClause){
		global $configs,$log,$thisfile;
		$con = conn_dbamsat();
		if(count($whereClause)==0){
			$whereClause=1;
		}
		else{
			$whereClause=implode(" AND ",$whereClause);
		}
		$result = mysqli_query($con,"SELECT * FROM $tblName WHERE $whereClause");
		closeConn_dbamsat($con);
		if($result==False){
			$log->logError("$thisfile: Query Failed : SELECT * FROM $tblName WHERE $whereClause");
		}
		else{
			$arr=array();
			while($row = mysqli_fetch_array($result)){
				$arr = array_merge($arr,array($row));
			}
			return $arr;
		}
	}
	
	function update_dbamsat($tblName, $setClause, $whereClause){
		global $configs,$log,$thisfile;
		$con = conn_dbamsat();
		if(count($setClause)==0){
			$log->logError("$thisfile: Update query is not possible without any SET.");
			return 0;
		}
		else{
			$setClause = implode(",",$setClause);
			if(count($whereClause)==0){
				$whereClause=1;
			}
			else{
				$whereClause=implode(" AND ",$whereClause);
			}
			$result=mysqli_query($con,"UPDATE $tblName SET $setClause WHERE $whereClause");
			if($result==False){
				$log->logError("$thisfile: Query Failed : UPDATE $tblName SET $setClause WHERE $whereClause");
			}
			closeConn_dbamsat($con);
			return $result;
		}
	}
	
	function insert_dbamsat($tblName,$setClause){
		global $configs,$log,$thisfile;
		$con =conn_dbamsat();
		if(count($setClause)==0){
			$log->logError("$thisfile: Insert query is not possible without any SET.");
			return -1;
		}
		else{
			$setClause= implode(",",$setClause);
			$result=mysqli_query($con,"INSERT INTO $tblName SET $setClause");
        }
        if($result==False){
			$log->logError("$thisfile: Query Failed : INSERT INTO $tblName SET $setClause");
		}
        closeConn_dbamsat($con);
	    return 1;
	}
	
	function delete_amsat($tblName,$whereClause){
		$con = conn_dbamsat();
		if(count($whereClause)==0){
			$whereClause='1';
		}
		else{
			$whereClause= implode(" AND ",$whereClause);
			$result=mysqli_query($con,"DELETE FROM $tblName WHERE $whereClause");
		}
		if($result==False){
			$log->logError("$thisfile: Query Failed : ");
		}
		closeConn_dbamsat($con);
	}
?>
