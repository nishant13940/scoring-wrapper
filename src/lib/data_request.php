<?php
	$configs = include realpath(dirname(__FILE__)).'/../config.php';
	require_once $configs['libPath'].'KLogger.php';
	require_once $configs['libPath'].'db.php';
	require_once $configs['libPath'].'file_opr.php';
	require_once $configs['srcPath'].'posting.php';
	
	$log = new KLogger ( $configs['dataPath'].'log/log.txt' , KLogger::DEBUG );	
	
	function request_question($candToProcess){
		global $configs, $log;
		$thisfile=basename(__FILE__,'');
		try{
			$template=select("template",array("name='questionFile'"));
			foreach($template as $key => $value){
				$value['text']=str_replace("###uid###",$candToProcess['quesId'],str_replace("###mid###",$candToProcess['moduleId'],str_replace("###sid###",$candToProcess['candId'],str_replace("###hit_url###",$configs['hit_url'],str_replace("<br/>","\r\n",$value['text'])))));
			}
			return $value['text'];
		}
		catch(Exception $e){
			$log->logError($candToPrcess['candId'].": $thisfile: Error creating question file. $e->getMessage()");
			return 0;
		}
	}
	
	function answerToPHPArray($xml){
		$answer = XML2Array::createArray($xml);
		$answer = $answer['QuestionFormAnswers']['Answer'];
		$answerPHP = array();
		foreach($answer as $val){
			$answerPHP[$val['QuestionIdentifier']] = $val['FreeText'];
		}
		return $answerPHP;
	}
	
	function getCreateHITParameters($candToProcess){
		global $log, $configs;
		
		$Worker_PercentAssignmentsApproved = array(
			"QualificationTypeId" => $configs['qual_percentApproved_id'],
			"Comparator"          => $configs['qual_percentApproved_comp'],
			"IntegerValue"        => $configs['qual_percentApproved_val']
		);
		
		$Worker_numberOfAssignmentsApproved = array(
			"QualificationTypeId" => $configs['qual_numberOfApproved_id'],
			"Comparator"          => $configs['qual_numberOfApproved_comp'],
			"IntegerValue"        => $configs['qual_numberOfApproved_val']
		);

		$Request = array(
			"Title"                       => $configs['hit_title'],
			"Description"                 => $configs['hit_description'],
			"Question"                    => request_question($candToProcess),
			"Reward"                      => array("Amount" => $candToProcess['reward'], "CurrencyCode" => "USD"),
			"MaxAssignments"			  => $candToProcess['numAssign'],
			"AssignmentDurationInSeconds" => $configs['assignmentDurationInSeconds'],
			"LifetimeInSeconds"           => $configs['lifetimeInSeconds'],
			"QualificationRequirement"    => array($Worker_PercentAssignmentsApproved, $Worker_numberOfAssignmentsApproved)
		);
		return $Request;
	}
	
	function getNotifyParameters($turkerId, $subject, $message){
		global $log, $configs;
		
		$Request = array(
			"Subject"					=>	$subject,
			"MessageText"				=>	$message,
			"WorkerId"					=>	$turkerId
		);
		return $Request;
	}
	
	function getForceExpireParameters($task){
		global $log, $configs;
		
		$Request = array(
			"HITId"						=>	$task['hitId']
		);
		return $Request;
	}
	
	function requestCandidateData($task, $isReposting){
		global $log, $configs;
		
		return array(
					"candId"			=>	$task['candidateId'],
					"moduleId"			=>	$task['moduleId'],
					"quesId"			=>	$task['questionId'],
					"reward"			=>	$task['pay'],
					"numAssign"			=>	$task['numOfAssignment'],
					"isReposting"		=>	$isReposting
				);
	}
	
	function getFetchAssignmentParameters($assignId){
		global $log, $configs;
		
		return array(
					"AssignmentId"		=>	$assignId
				);
	}
	
	function getApproveAssignmentParameters($assignId){
		global $log, $configs;
		
		return array(
					"AssignmentId"		=>	$assignId,
					"RequesterFeedback"	=>	$configs['ApproveFeedback']
					
				);
	}
	
	function getRejectAssignmentParameters($assignId){
		global $log, $configs;
		
		return array(
					"AssignmentId"		=>	$assignId,
					"RequesterFeedback"	=>	$configs['RejectFeedback']
					
				);
	}
	
	function getExtendHITParameters($HITId, $numOfAssignmentsToIncrease){
		global $log, $configs;
		
		return array(
					"HITId"						=>	$HITId,
					"MaxAssignmentsIncrement"	=>	$numOfAssignmentsToIncrease
				);
	}
?>
