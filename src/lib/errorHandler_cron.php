<?php
	$thisfile=basename(__FILE__, '');
	$configs = include realpath(dirname(__FILE__)).'/../config.php';
	require_once $configs['libPath'].'KLogger.php';
	require_once $configs['libPath'].'db_amsat.php';
	require_once $configs['libPath'].'db.php';
	require_once $configs['srcPath'].'callCSDelivery.php';
	require_once $configs['srcPath'].'callSVARScoring.php';
	require_once $configs['libPath'].'scoringStatus.php';
	require_once $configs['libPath'].'notifyError.php';
	
	set_time_limit($configs['php_time_limit']);

	$log = new KLogger ( $configs['dataPath'].'log/log.txt' , KLogger::DEBUG );
	$thisfile=basename(__FILE__, '');

	postIdsInBuffer();
	//postSVARResultNotProcessedIds();
	//postCSResultNotProcessedIds();

	function postIdsInBuffer(){
		global $log, $configs,$thisfile;
		$AllIds = select("postingBuffer",array());
		$host_machine_addr = $configs['host_machine_addr'];
		$port = $configs['port'];
		$scoring_machine_addr = $configs['scoring_machine_addr'];
		foreach( $AllIds as $candId){
			delete("postingBuffer",array(
									"candidateId='".$candId['candidateId']."'"
									));
			if($candId['attempts']<$configs['MaximumRetryConnectionAttempt']){
				$isSuccessfullyCalled = call_cs_delivery_func($configs['cs_delivery_host_port'], $host_machine_addr,$configs['cs_delivery_host_addr'], $candId['candidateId']);
				if($isSuccessfullyCalled){
					$log->logInfo($candId['candidateId'].": $thisfile: Candidate written on cs delivery port successfully.");
				}
				else{
					insert("postingBuffer",array(
										"candidateId='".$candId['candidateId']."'",
										"attempts='0'",
										"raisedAt=NOW()"
										));
					$log->logError($candId['candidateId'].": $thisfile: Could not write candidate information on cs delivery port.");			
				}
			}
		}
	}
	
	function postSVARResultNotProcessedIds(){
		global $log, $configs;
		$thisfile=basename(__FILE__, '');
		
		$SVARCallingArray = select("scoringStatus", array(
													"SVAR='".$configs['scoringStatus_RSLRScoringCalling']."'",
													"updateTimeStamp > NOW() - INTERVAL ".$configs['wrapper_lookBackHoursForScoringCron']." HOUR"));
		$SVARCalledArray = select("scoringStatus", array(
													"SVAR='".$configs['scoringStatus_scoreUpdate']."'",
													"updateTimeStamp > NOW() - INTERVAL ".$configs['wrapper_lookBackHoursForScoringCron']." HOUR"));
		foreach($SVARCallingArray as $ids){
			$SVARCallingids[] = $ids['candidateId'];
		}
		foreach($SVARCalledArray as $ids){
			$SVARCalledids[] = $ids['candidateId'];
		}
		$idsToCallSVARFor = array_unique(array_diff($SVARCallingids, $SVARCalledids));
		if(($num = count($idsToCallSVARFor)) > $configs['minNumOfIdsStuckToInform']){
			$subject= "Scoring Wrapper Warning | SVAR Scoring not responding ";
			$message = "$thisfile: SVAR scoring is not responding to wrapper. There are huge number of ids pending to be scored. Please check if SVAR Scoring is running. Pending Ids count : ".$num;
			notifyHumans($subject, $message);
			$log->logError($message);
		}
		foreach($idsToCallSVARFor as $input_dec){
			updateScoringStatus($input_dec, $configs['scoringStatus_forRSLR'], $configs['scoringStatus_RSLRScoringCalling'], "");
			$isSuccessfullyCalled = call_svar_scoring_func($configs['svar_scoring_host_port'], $configs['host_machine_addr'],$configs['svar_scoring_host_addr'], $input_dec);
			if($isSuccessfullyCalled){
				updateScoringStatus($input_dec, $configs['scoringStatus_forRSLR'], $configs['scoringStatus_RSLRScoringCalled'], "");
				$log->logInfo($input_dec.": $thisfile: Candidate written on svar scoring port successfully.");
			}
			else{
				$log->logError($input_dec.": $thisfile: Could not write candidate information svar scoring port.");
			}
		}
	}
	
	function postCSResultNotProcessedIds(){
		global $log, $configs;
		$thisfile=basename(__FILE__, '');
		
		$CSCallingArray = select("scoringStatus", array(
													"FS='".$configs['scoringStatus_CSDeliveryDone']."'",
													"updateTimeStamp > NOW() - INTERVAL ".$configs['wrapper_lookBackHoursForScoringCron']." HOUR"));
		$CSCalledArray = select("scoringStatus", array(
													"FS='".$configs['scoringStatus_scoreUpdate']."'",
													"updateTimeStamp > NOW() - INTERVAL ".$configs['wrapper_lookBackHoursForScoringCron']." HOUR"));
		$CSCallingids=array();
		foreach($CSCallingArray as $ids){
			$CSCallingids[] = $ids['candidateId'];
		}
		$scoreUpdatedids=array();
		foreach($CSCalledArray as $ids){
			$scoreUpdatedids[] = $ids['candidateId'];
		}
		$idsToCallCSScoringFor = array_unique(array_diff($CSCallingids, $scoreUpdatedids));
		print_r($idsToCallCSScoringFor);
		if(($num = count($idsToCallCSScoringFor)) > $configs['minNumOfIdsStuckToInform']){
			$subject= "Scoring Wrapper Warning | CS Scoring not responding ";
			$message = "$thisfile: CS scoring is not responding to wrapper. There are huge number of ids pending to be scored. Please check if CS Scoring is running. Pending Ids count : ".$num;
			notifyHumans($subject, $message);
			$log->logError($message);
		}
		foreach($idsToCallCSScoringFor as $input_dec){
			updateScoringStatus($input_dec, $configs['scoringStatus_forFS'], $configs['scoringStatus_CSDeliveryCalling'], "");
			$isSuccessfullyCalled = call_cs_delivery_func($configs['cs_delivery_host_port'], $configs['host_machine_addr'],$configs['cs_delivery_host_addr'], $input_dec);
			if($isSuccessfullyCalled){
				updateScoringStatus($input_dec, $configs['scoringStatus_forFS'], $configs['scoringStatus_CSDeliveryCalled'], "");
				$log->logInfo($input_dec.": $thisfile: Candidate written on cs delivery port successfully.");
			}
			else{
				$log->logError($input_dec.": $thisfile: Could not write candidate information on cs delivery port.");
			}
		}
	}
?>
