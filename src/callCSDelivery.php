<?php
	$thisfile=basename(__FILE__, '');
	$configs = include realpath(dirname(__FILE__)).'/config.php';
	require_once $configs['libPath'].'KLogger.php';
	require_once $configs['libPath'].'db_amsat.php';
	require_once $configs['libPath'].'db.php';
	require_once $configs['libPath'].'notifyError.php';
	set_time_limit($configs['php_time_limit']);

	$log = new KLogger ( $configs['dataPath'].'log/log.txt' , KLogger::DEBUG );

	function call_cs_delivery_func($port, $host_machine_addr, $scoring_machine_addr, $candId)
	{
		global $log;
		$thisfile=basename(__FILE__, '');
		$socket = socket_create(AF_INET, SOCK_STREAM, 0);

		if($socket == False){
			$log->logError($candId.": $thisfile: Socket couldn't be created.");
			$subject= "Crowdsource Error | Error writing on candidate on cs delivery for ".$candId;
			$message = $candId.": $thisfile: Error writing candidate on cs delivery port. Socket could not be created. Skipping Candidate.";
			notifyHumans($subject, $message);
			$log->logError($message);
			insert("error", array(
									"RaisedAt=NOW()",
									"QueryString=''",
									"Message='".$message."'"
									));
			return 0;
		}
		else{
			$log->logInfo($candId.": $thisfile: Socket created successfully.");
			$isSocketConnected =socket_connect($socket, $scoring_machine_addr, $port);
			if(!$isSocketConnected){
				$subject= "Crowdsource Error | Error writing on candidate on cs delivery for ".$candId;
				$message = $candId.": $thisfile: Error writing candidate on port. Skipping Candidate.";
				notifyHumans($subject, $message);
				$log->logError($message);
				insert("error", array(
										"RaisedAt=NOW()",
										"QueryString=''",
										"Message='".$message."'"
										));
				insert("postingBuffer",array(
										"candidateId='".$candId."'",
										"attempts='0'",
										"raisedAt=NOW()"
										));
				return 0;
			}
			else{
				$candArr = constructCandArray($candId);
				socket_write($socket,$candArr);
				$log->logInfo($candId.": $thisfile: candidate written on port successfully");
				sleep(1);
				socket_close($socket);
				return 1;
			}
		}
	}

	function casttoclass($class, $object){
		return unserialize(preg_replace('/^O:\d+:"[^"]++"/', 'O:' . strlen($class) . ':"' . $class . '"', serialize($object)));
	}
	
	function constructCandArray($candId){
		global $log,$configs;
		$tioContent =	file_get_contents($configs['candidateDataServer'].$candId."/".$candId.".tio");
		if($tioContent != False){
			$tioArray 	=	unserialize($tioContent);
			$tioArray = casttoclass('stdClass', $tioArray);
			$tioArray = $tioArray->testResults;
			$temp = $tioArray[sizeof($tioArray) - 1];
			foreach($tioArray as $val){
				if($val['subcategoryID']==$configs['subcategoryID']){
					$temp = $val;
				}
			}
			$tioArray = $temp;
			if($tioArray['subcategoryID']!=$configs['subcategoryID']){
				$subject = "Scoring Wrapper | Candidate tio doesn't have FS SubCategoryID";
				$message = $candId.": Candidate tio doesn't have FS SubCategoryID.";
				$log->logError($message);
				notifyHumans($subject, $message);
				$errorCandArr = createErrorCandArr($candId);
				return serialize($errorCandArr);
			}
			else{
				$moduleId 	= 	$tioArray['moduleID'];
				$quesId		=	$tioArray['questionID'];
				$isReposting	=	"0";
				$tpodId	=	substr($candId, 0, 8);
				$HITTypeId = select("tpodIdMapping",array("TPODId='".$tpodId."'"));
				if(empty($HITTypeId)){
					$HITTypeId = select("tpodIdMapping",array("TPODId='default'"));
				}
				$HITTypeId	=	$HITTypeId[0];
				$HITDetails	= 	select("HITDetails",array("HITTypeId='".$HITTypeId['HITTypeId']."'"));
				$HITDetails	=	$HITDetails[0];
				$candArr = array(
							"candId"=>$candId,
							"moduleId"=>$moduleId,
							"quesId"=>$quesId,
							"isReposting"=>$isReposting,
							"reward"=>$HITDetails['reward'],
							"numAssign"=>$HITDetails['numAssign']
							);
				return serialize($candArr);
			}
		}
		else{
			$subject = "Scoring Wrapper | Could not find tio in candidateDataServer.";
			$message = $candId.": Could not find candidate tio in CadindateDataServer";
			$log->logError($message);
			notifyHumans($subject, $message);
			$errorCandArr = createErrorCandArr($candId);
			return serialize($errorCandArr);
		}
	}
	
	function createErrorCandArr($candId){
			return array(
						"candId"=>$candId,
						"error"=>""
			);
	}
	
?>
